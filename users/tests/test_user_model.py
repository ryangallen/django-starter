from django.test import TestCase

from users.models import User


class UserModelTest(TestCase):

    def test_create_user_only_requires_email(self):
        user = User.objects.create_user(email='hi@example.com')
        self.assertEqual(str(user), 'hi@example.com')
