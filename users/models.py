from django.conf import settings
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser, Group
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _


class UserManager(BaseUserManager):
    # https://github.com/django/django/blob/master/django/contrib/auth/base_user.py#L16
    """Define a model manager for User model with no username field."""
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a user with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True')

        return self._create_user(email, password, **extra_fields)

    def onboard(self, email, **extra_fields):
        if extra_fields.get('password'):
            raise ValueError('User onboard does not accept a password. A link to set '
                             'a password will be sent to the email address provided.')
        password = self.make_random_password(length=20)
        user = (self.filter(email__iexact=email, is_active=True).first() or
                self.create_user(email, password, **extra_fields))

        groups = extra_fields.pop('groups', [])
        groups = [Group.objects.get_or_create(name=group)[0] for group in groups]
        user.groups.set(groups)

        EmailMultiAlternatives(
            'Please set a new password for your {} account'.format(settings.PROJECT_NAME),
            loader.render_to_string('registration/onboarding_password_reset_email.html', {
                'protocol': 'https',
                'domain': settings.HOST,
                'user': user,
                'email': user.email,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token': default_token_generator.make_token(user),
            }),
            settings.DEFAULT_FROM_EMAIL,
            [user.email]
        ).send()
        return user


class User(AbstractUser):
    # https://github.com/django/django/blob/master/django/contrib/auth/models.py#L288
    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def save(self, *args, **kwargs):
        self.email = self.email.lower()
        super().save(*args, **kwargs)
