from django.contrib import admin
from django.contrib.auth.admin import (UserAdmin as DefaultUserAdmin,
                                       GroupAdmin as DefaultGroupAdmin)
from django.contrib.auth.models import Group
from django.utils.translation import gettext, gettext_lazy as _

from .forms import GroupAdminForm
from .models import User

admin.site.unregister(Group)


@admin.register(User)
class UserAdmin(DefaultUserAdmin):
    # https://github.com/django/django/blob/master/django/contrib/auth/admin.py#L42
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff', 'is_superuser', 'is_active',)
    list_filters = ('is_staff', 'is_superuser', 'is_active',)
    ordering = ('email',)


@admin.register(Group)
class GroupAdmin(DefaultGroupAdmin):
    # https://github.com/django/django/blob/master/django/contrib/auth/admin.py#L26
    form = GroupAdminForm
