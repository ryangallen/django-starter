#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
PROJECT_NAME='django_starter'
DB_NAME=$PROJECT_NAME'_db'

sudo -E apt-get update
sudo -E apt-get install -y software-properties-common screen git nginx
sudo -E apt-get install -y dirmngr --install-recommends

# Install Python 3
if ! command -v python3.6; then
    sudo add-apt-repository 'deb http://ftp.de.debian.org/debian testing main'
    echo 'APT::Default-Release "stable";' | sudo tee -a /etc/apt/apt.conf.d/00local
    sudo -E apt-get update
    sudo -E apt-get -t testing install -y python3.6 python3-pip
fi

# Install OS user pip packages
if ! command -v pipenv; then
    python3.6 -m pip install --user pipenv
fi

# Install PostgreSQL and create database
if ! command -v psql; then
    sudo -E apt-get install -y postgresql libpq-dev
    sudo su - postgres --command 'createuser -s vagrant'
    createdb $DB_NAME
fi

# Create development SSL key and certificate
cd /vagrant/conf/ssl
openssl req -x509 -nodes -days 365 -newkey rsa -config /vagrant/conf/ssl/dev.conf \
                                               -keyout /vagrant/conf/ssl/dev.key \
                                               -out /vagrant/conf/ssl/dev.crt \
                                               2>/dev/null # supress output

# Set up environment
if [ ! -f /vagrant/.env ]; then
  cp /vagrant/conf/template.env /vagrant/.
fi
cp /vagrant/conf/.bashrc /home/vagrant/.

# Set up nginx/SSL
sudo ln -sf /vagrant/conf/ssl/* /etc/ssl/.
sudo rm /etc/nginx/sites-enabled/*
sudo ln -sf /vagrant/conf/nginx/sites-enabled/* /etc/nginx/sites-enabled/.
sudo ln -sfd /vagrant/.static /var/static
sudo rm -r /var/www
sudo ln -sfd /vagrant/conf/nginx/try_files /var/www

# Set environment vars
export PATH=$PATH:/home/vagrant/.local/bin
export PIPENV_VENV_IN_PROJECT=true

# Launch application
cd /vagrant
pipenv install --dev
pipenv run python manage.py migrate --no-input
pipenv run python manage.py collectstatic --no-input
screen -dmS $PROJECT_NAME pipenv run python manage.py runserver 0.0.0.0:8000
sudo systemctl start nginx
