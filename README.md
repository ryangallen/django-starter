# Django Starter

A Debian VM environment for building a Django project with Python 3.

## Set up
1. Install [Vagrant](https://www.vagrantup.com/downloads.html)
1. Clone this repository and launch the development VM:

        $ git clone git@<repository_host>:django-starter.git
        $ cd django-starter
        $ vagrant up

1. View the running web application in a browser at http://172.30.0.80/

## Development

- Use `vagrant ssh` to access the VM shell
- [Pipenv](https://pypi.org/project/pipenv/) is already installed on the VM and
  ready to use. You do not need to install or use virtualenv/virtualenvwrapper
- Use `pipenv run` to run a given command from the virtualenv, with any
  arguments forwarded
- Use `pipenv install [OPTIONS] [PACKAGE_NAME]` to install additional packages
  in the virtualenv (Use the `--dev` option if the dependency is only needed in
  development)

### Testing

Tests can be run using pytest:

    $ pipenv run pytest

Use the `-s` option to show `print` statements in the test output.

### Environment variables

A `.env` file is created when you first call `vagrant up` and should be
edited to match your project specifications. Be sure to change the `SECRET_KEY`
value for any production-like environments.

### Server Logs

The Django development server is running in a separate terminal session through
[GNU Screen](https://www.linode.com/docs/networking/ssh/using-gnu-screen-to-manage-persistent-terminal-sessions/).

- To view the Django server logs, run `screen -r`
- To exit the logs without killing the server, press **Ctrl + A + D**
- To restart the server if you kill it, run
  `screen -dmS django_starter pipenv run python manage.py runserver 0.0.0.0:8000`

### SSL in Development

In order to develop in a production-like as possible environment a self-signed
SSL certificate can be used. The dev key and certificate are already setup but
the steps to create them are documented below for reference.

To create a self-signed key and certificate pair, use the OpenSSL
[`req` command](https://www.openssl.org/docs/manmaster/man1/req.html) to create
the `.key` and `.crt` files.

    $ openssl req -x509 -nodes -days 365 -newkey rsa -config dev.conf -keyout dev.key -out dev.crt
