from django.conf import settings

import logging
logger = logging.getLogger('simple')


def project(request):
    return {
        'project': {
            'name': settings.PROJECT_NAME,
            'title': settings.PROJECT_TITLE,
        },
        'release': {
            'branch': settings.BRANCH,
            'commit': settings.COMMIT,
        }
    }
