import subprocess


def subprocess_output(args):
    try:
        return subprocess.check_output(args).decode('utf-8').strip()
    except (FileNotFoundError, subprocess.CalledProcessError):
        return ''
