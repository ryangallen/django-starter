import os
import sys

from django.utils.log import DEFAULT_LOGGING

from .utils import subprocess_output

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

PROJECT_NAME = os.getenv('PROJECT_NAME', '')
PROJECT_TITLE = PROJECT_NAME.replace('_', ' ').title()
BRANCH = subprocess_output(['git', 'rev-parse', '--abbrev-ref', 'HEAD'])
COMMIT = subprocess_output(['git', 'rev-parse', '--short', 'HEAD'])

SECRET_KEY = os.getenv('SECRET_KEY')
ALLOWED_HOSTS = os.getenv('ALLOWED_HOSTS', '').split(',')
HOST = os.getenv('HOST')
DEBUG = os.getenv('DEBUG') in [True, 'True', 'true']
TESTING = 'test' in sys.argv

ADMINS = [
    # ('Admin Name', 'admin@example.com'),
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'users',
]

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = LOGIN_REDIRECT_URL
LOGIN_URL = '/login'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'django_starter.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'django_starter/templates/'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django_starter.context_processors.project',
            ],
        },
    },
]

WSGI_APPLICATION = 'django_starter.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': os.getenv('POSTGRES_HOST') or os.getenv('RDS_HOSTNAME'),
        'PORT': os.getenv('POSTGRES_PORT') or os.getenv('RDS_PORT'),
        'NAME': os.getenv('POSTGRES_DB') or os.getenv('RDS_DB_NAME'),
        'USER': os.getenv('POSTGRES_USER') or os.getenv('RDS_USERNAME'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD') or os.getenv('RDS_PASSWORD'),
    }
}

AUTH_USER_MODEL = 'users.User'
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, '.static')
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'django_starter/static/'),)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend' if DEBUG else \
                'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = os.getenv('EMAIL_HOST')
EMAIL_PORT = os.getenv('EMAIL_PORT')
EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'noreply@{host}'.format(host=EMAIL_HOST)

# # # Setting up loggers never made sense to me until I read this tutorial:
# # # https://www.webforefront.com/django/setupdjangologging.html
# LOGGING = DEFAULT_LOGGING
# file_handler = {
#     'level': 'DEBUG' if DEBUG else 'ERROR',
#     'class': 'logging.handlers.RotatingFileHandler',
#     'maxBytes': 1024 * 1024 * 100, # 100MB
#     'backupCount': 5,
#     'formatter': 'django.server'
# }
# LOGGING['handlers']['django.file'] = {
#     **file_handler, **{'filename': '/var/log/django.log'}
# }
# LOGGING['handlers']['django.server.file'] = {
#     **file_handler, **{'filename': '/var/log/django.server.log'}
# }
# LOGGING['loggers']['django']['handlers'] += ['django.file']
# LOGGING['loggers']['django.server']['handlers'] += ['django.server.file']
